package com.mahisoft.tutorial.service.controller;

import com.google.common.base.Strings;
import com.mahisoft.kamino.commonbeans.exception.ApiHttpClientErrorException;
import com.mahisoft.tutorial.service.controller.dto.*;
import org.apache.commons.lang3.tuple.Triple;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TutorialControllerTest {
    @LocalServerPort
    private int port;

    @Value("${server.contextPath}")
    private String contextPath;

    @Autowired
    private RestTemplate restTemplate;

    @Test
    public void getGreetingTest() {
        String person = "Peter Parker";
        String text = restTemplate.getForObject(String.format("http://localhost:%s%s/v1/tutorial/greeting/{person}", port, contextPath), String.class, person);

        Assert.assertEquals("Result doesn't match","Hello World Peter Parker!", text);
    }

    @Test
    public void getAllProducts() {
        CreateProductRequest product1 = CreateProductRequest
                .builder()
                .name("Product 1")
                .category("Category 1")
                .price(BigDecimal.valueOf(1000L))
                .discount(25D)
                .build();
        CreateProductRequest product2 = CreateProductRequest
                .builder()
                .name("Product 2")
                .category("Clearance")
                .discount(75D)
                .price(BigDecimal.valueOf(1000L))
                .build();

        Long idProduct1 = createProduct(product1).getBody();
        Long idProduct2 = createProduct(product2).getBody();

        ResponseEntity<ProductItemList> response = restTemplate.getForEntity(String.format("http://localhost:%s%s/v1/tutorial/", port, contextPath), ProductItemList.class);

        Assert.assertEquals("Response status doesn't match", response.getStatusCode(), HttpStatus.OK);

        Map<Long, ProductItem> products = response.getBody().getProducts().stream().collect(
                Collectors.toMap(product -> product.getId(), product -> product)
        );

        Assert.assertTrue(products.size() >= 2);
        Assert.assertEquals(product1.getName(), products.get(idProduct1).getName());
        Assert.assertEquals(product2.getName(), products.get(idProduct2).getName());
    }

    @Test
    public void getProductInfoTest() {
        Long id = createProduct(CreateProductRequest
                .builder()
                .name("Test Product")
                .category("Clearance")
                .discount(25D)
                .price(BigDecimal.valueOf(100L))
                .build()
        ).getBody();

        ResponseEntity<ProductItem> response = getProduct(id);

        Assert.assertEquals("Response status doesn't match", HttpStatus.OK, response.getStatusCode());
        Assert.assertNotNull("ProductInfo is null", response.getBody());

        Assert.assertFalse("Name is null or empty", Strings.isNullOrEmpty(response.getBody().getName()));
        Assert.assertFalse("Category is null or empty", Strings.isNullOrEmpty(response.getBody().getCategory()));
        Assert.assertEquals("Price doesn't match", BigDecimal.valueOf(100L), noDecimalsRoundHalfUp(response.getBody().getPrice()));
        Assert.assertEquals("Discount doesn't match", 25D, response.getBody().getDiscount(), 0D);
    }

    @Test(expected = ApiHttpClientErrorException.class)
    public void getProductInfoFailsNotFoundTest() {
        try {
            Long id = 999999999999999L;
            restTemplate.getForObject(String.format("http://localhost:%s%s/v1/tutorial/{id}", port, contextPath), Long.class, id);
            Assert.fail("ApiHttpClientErrorException Expected");
        }
        catch (ApiHttpClientErrorException e) {
            Assert.assertEquals("Response status doesn't match", HttpStatus.NOT_FOUND, e.getStatusCode());
            throw e;
        }
    }

    @Test
    public void createProductTest() {
        CreateProductRequest request = CreateProductRequest
                .builder()
                .name("Test Product")
                .price(BigDecimal.valueOf(100L))
                .category("Clearance")
                .discount(25D)
                .build();

        ResponseEntity<Long> response = restTemplate.postForEntity(String.format("http://localhost:%s%s/v1/tutorial", port, contextPath), request,
                Long.class);

        Assert.assertEquals("Response status doesn't match", HttpStatus.CREATED, response.getStatusCode());
        Assert.assertNotNull("Response value is null", response.getBody());
        Assert.assertTrue("The Id should be greater that 0", response.getBody() > 0);
    }

    @Test
    public void createProductValidationFailTest() throws IOException {
        List<Triple<String, List<String>, CreateProductRequest>> scenarios =
                Arrays.asList(
                        // Scenario 1, all fields are null
                        Triple.of(
                                "Required field validation: %s",
                                Arrays.asList("Name is required", "Price is required", "Category is required"),
                                CreateProductRequest.builder().build()
                        ),
                        //Scenario 2, all fields are supplied but don't follow requirements
                        Triple.of(
                                "Constrain field validation: %s",
                                Arrays.asList(
                                        "Name must have between 3 and 25 characters",
                                        "Category must have between 3 and 25 characters",
                                        "Discount min value is 10"),
                                CreateProductRequest.builder()
                                        .name("A")
                                        .price(BigDecimal.valueOf(10L))
                                        .discount(1D)
                                        .category("A")
                                        .build()
                        ),
                        // Scenario 3, test custom validator for discount
                        Triple.of(
                                "Custom constrain validation: %s",
                                Collections.singletonList(
                                        "Discount is required when the category is Clearance"),
                                CreateProductRequest
                                        .builder()
                                        .name("Test Product")
                                        .price(BigDecimal.valueOf(10L))
                                        .category("Clearance")
                                        .build()
                        )
                );

        for (Triple<String, List<String>, CreateProductRequest> scenario : scenarios) {

            try {
                restTemplate.postForEntity(String.format("http://localhost:%s%s/v1/tutorial", port, contextPath), scenario.getRight(),
                        Long.class);
                Assert.fail("ApiHttpClientErrorException Expected");
            } catch (ApiHttpClientErrorException e) {
                Assert.assertEquals("Response status doesn't match", HttpStatus.BAD_REQUEST, e.getStatusCode());
                for (String expectedError : scenario.getMiddle()) {
                    // The response is a big JSON string and parse it might need too much effort just for testing
                    // so in this case we simply check if the message we put in the annotation is present in that JSON
                    Assert.assertTrue(
                            String.format(scenario.getLeft(), expectedError),
                            e.getResponseBodyAsString().indexOf(expectedError) > 0
                    );
                }
            }
        }
    }

    @Test
    public void updateProduct() {
        Long id = createProduct(CreateProductRequest
                .builder()
                .name("Product Test")
                .price(BigDecimal.valueOf(100L))
                .category("Category Test")
                .discount(25D)
                .build()
        ).getBody();

        UpdateProductRequest updateRequest = UpdateProductRequest
                .builder()
                .name("New Product Test")
                .price(BigDecimal.valueOf(101L))
                .category("New Category Test")
                .discount(25D)
                .status(StatusType.Inactive)
                .build();

        ResponseEntity<Void> response = updateProduct(id, updateRequest);

        Assert.assertEquals("Response status doesn't match", HttpStatus.NO_CONTENT, response.getStatusCode());

        ProductItem productResponse = getProduct(id).getBody();

        Assert.assertEquals("Name doesn't match", updateRequest.getName(), productResponse.getName());
        Assert.assertEquals("Category doesn't match", updateRequest.getCategory(), productResponse.getCategory());
        Assert.assertEquals("Price doesn't match", updateRequest.getPrice(), noDecimalsRoundHalfUp(productResponse.getPrice()));
        Assert.assertEquals("Discount doesn't match", updateRequest.getDiscount(), productResponse.getDiscount());
        Assert.assertEquals("Status doesn't match", updateRequest.getStatus(), productResponse.getStatus());
    }

    @Test
    public void updateProductValidationFailTest() throws IOException {
        Long id = createProduct(CreateProductRequest
                .builder()
                .name("Product Test")
                .price(BigDecimal.valueOf(100L))
                .category("Category Test")
                .discount(25D)
                .build()
        ).getBody();

        List<Triple<String, List<String>, UpdateProductRequest>> scenarios =
                Arrays.asList(
                        // Scenario 1, all fields are null
                        Triple.of(
                                "Required field validation: %s",
                                Arrays.asList("Name is required", "Price is required", "Category is required", "Status is required"),
                                UpdateProductRequest.builder().build()
                        ),
                        //Scenario 2, all fields are supplied but don't follow requirements
                        Triple.of(
                                "Constrain field validation: %s",
                                Arrays.asList(
                                        "Name must have between 3 and 25 characters",
                                        "Category must have between 3 and 25 characters",
                                        "Discount min value is 10"),
                                UpdateProductRequest.builder()
                                        .name("A")
                                        .price(BigDecimal.valueOf(10L))
                                        .discount(1D)
                                        .category("A")
                                        .status(StatusType.Inactive)
                                        .build()
                        ),
                        // Scenario 3, test custom validator for discount
                        Triple.of(
                                "Custom constrain validation: %s",
                                Collections.singletonList(
                                        "Discount is required when the category is Clearance"),
                                UpdateProductRequest
                                        .builder()
                                        .name("Test Product")
                                        .price(BigDecimal.valueOf(10L))
                                        .category("Clearance")
                                        .status(StatusType.Active)
                                        .build()
                        )
                );

        for (Triple<String, List<String>, UpdateProductRequest> scenario : scenarios) {

            try {
                updateProduct(id, scenario.getRight());
                Assert.fail("ApiHttpClientErrorException Expected");
            } catch (ApiHttpClientErrorException e) {
                Assert.assertEquals("Response status doesn't match", HttpStatus.BAD_REQUEST, e.getStatusCode());
                for (String expectedError : scenario.getMiddle()) {
                    // The response is a big JSON string and parse it might need too much effort just for testing
                    // so in this case we simply check if the message we put in the annotation is present in that JSON
                    Assert.assertTrue(
                            String.format(scenario.getLeft(), expectedError),
                            e.getResponseBodyAsString().indexOf(expectedError) > 0
                    );
                }
            }
        }
    }

    @Test
    public void updateProductNotFoundId() throws IOException {
        try {
            Long id = 999999999999999L;
            UpdateProductRequest updateRequest = UpdateProductRequest
                    .builder()
                    .name("New Product Test")
                    .price(BigDecimal.valueOf(101L))
                    .category("New Category Test")
                    .discount(10D)
                    .status(StatusType.Inactive)
                    .build();
            updateProduct(id, updateRequest);
            Assert.fail("ApiHttpClientErrorException Expected");
        } catch (ApiHttpClientErrorException e) {
            Assert.assertEquals("Response status doesn't match", HttpStatus.NOT_FOUND, e.getStatusCode());
        }
    }

    @Test
    public void partialUpdateProduct() {
        CreateProductRequest original = CreateProductRequest
                .builder()
                .name("Product Test")
                .price(BigDecimal.valueOf(100L))
                .category("Clearance")
                .discount(25D)
                .build();
        Long id = createProduct(original).getBody();

        PartialUpdateProductRequest updateRequest = PartialUpdateProductRequest
                .builder()
                .name("Product Updated")
                .build();

        ResponseEntity<Void> response = updatePartialProduct(id, updateRequest);

        Assert.assertEquals("Response status doesn't match", response.getStatusCode(), HttpStatus.NO_CONTENT);

        ProductItem productUpdated = getProduct(id).getBody();

        Assert.assertEquals("Name doesn't match", updateRequest.getName(), productUpdated.getName());
        Assert.assertEquals("Category doesn't match", original.getCategory(), productUpdated.getCategory());
        Assert.assertEquals("Price doesn't match", original.getPrice(), noDecimalsRoundHalfUp(productUpdated.getPrice()));
        Assert.assertEquals("Discount doesn't match", original.getDiscount(), productUpdated.getDiscount());
    }

    @Test
    public void partialUpdateProductValidationFailTest() throws IOException {
        Long id = createProduct(CreateProductRequest
                .builder()
                .name("Product Test")
                .price(BigDecimal.valueOf(100L))
                .category("Category Test")
                .discount(25D)
                .build()
        ).getBody();

        List<Triple<String, List<String>, PartialUpdateProductRequest>> scenarios =
                Arrays.asList(
                        //Scenario 1, fields don't follow requirements
                        Triple.of(
                                "Constrain field validation: %s",
                                Arrays.asList(
                                        "Name must have between 3 and 25 characters",
                                        "Category must have between 3 and 25 characters",
                                        "Discount min value is 10"),
                                PartialUpdateProductRequest.builder()
                                        .name("A")
                                        .price(BigDecimal.valueOf(10L))
                                        .discount(1D)
                                        .category("A")
                                        .build()
                        ),
                        // Scenario 2, test custom validator for discount
                        Triple.of(
                                "Custom constrain validation: %s",
                                Collections.singletonList(
                                        "Discount is required when the category is Clearance"),
                                PartialUpdateProductRequest
                                        .builder()
                                        .category("Clearance")
                                        .discount(null)
                                        .build()
                        )
                );

        for (Triple<String, List<String>, PartialUpdateProductRequest> scenario : scenarios) {

            try {
                updatePartialProduct(id, scenario.getRight());
                Assert.fail("ApiHttpClientErrorException Expected");
            } catch (ApiHttpClientErrorException e) {
                Assert.assertEquals("Response status doesn't match", HttpStatus.BAD_REQUEST, e.getStatusCode());
                for (String expectedError : scenario.getMiddle()) {
                    // The response is a big JSON string and parse it might need too much effort just for testing
                    // so in this case we simply check if the message we put in the annotation is present in that JSON
                    Assert.assertTrue(
                            String.format(scenario.getLeft(), expectedError),
                            e.getResponseBodyAsString().indexOf(expectedError) > 0
                    );
                }
            }
        }
    }

    @Test
    public void partialUpdateProductNotFoundId() throws IOException {
        try {
            Long id = 999999999999999L;
            PartialUpdateProductRequest updateRequest = PartialUpdateProductRequest
                    .builder()
                    .name("New Product Test")
                    .build();
            updatePartialProduct(id, updateRequest);
            Assert.fail("ApiHttpClientErrorException Expected");
        } catch (ApiHttpClientErrorException e) {
            Assert.assertEquals("Response status doesn't match", HttpStatus.NOT_FOUND, e.getStatusCode());
        }
    }

    @Test
    public void deleteProduct() throws IOException  {
        Long id = createProduct(CreateProductRequest
                .builder()
                .name("Product Name")
                .category("Category Test")
                .discount(25D)
                .price(BigDecimal.valueOf(1000L))
                .build()
        ).getBody();
        PartialUpdateProductRequest inactiveProduct = PartialUpdateProductRequest
                .builder()
                .status(StatusType.Inactive)
                .build();

        updatePartialProduct(id, inactiveProduct);

        ResponseEntity<Void> response = deleteProduct(id);

        Assert.assertEquals("Response doesn't match", response.getStatusCode(), HttpStatus.NO_CONTENT);

        try {
            getProduct(id);
            Assert.fail("ApiHttpClientErrorException Expected");
        } catch (ApiHttpClientErrorException e) {
            Assert.assertEquals("Response status doesn't match", HttpStatus.NOT_FOUND, e.getStatusCode());
        }
    }

    @Test
    public void deleteProductValidationFailTest() throws IOException  {
        Long id = createProduct(CreateProductRequest
                .builder()
                .name("Product Name")
                .category("Category Test")
                .price(BigDecimal.valueOf(1000L))
                .discount(25D)
                .build()
        ).getBody();

        try {
            deleteProduct(id);
            Assert.fail("ApiHttpClientErrorException Expected");
        } catch (ApiHttpClientErrorException e) {
            Assert.assertEquals("Response status doesn't match", HttpStatus.PRECONDITION_FAILED, e.getStatusCode());
        }
    }

    @Test
    public void deleteProductProductNotFound() throws IOException {
        try {
            Long id = 999999999999999L;
            deleteProduct(id);
            Assert.fail("ApiHttpClientErrorException Expected");
        } catch (ApiHttpClientErrorException e) {
            Assert.assertEquals("Response status doesn't match", HttpStatus.NOT_FOUND, e.getStatusCode());
        }
    }

    private BigDecimal noDecimalsRoundHalfUp(BigDecimal d) {
        return d.setScale(0, BigDecimal.ROUND_HALF_UP);
    }

    private ResponseEntity<Long> createProduct(CreateProductRequest request) {
        return restTemplate.postForEntity(String.format("http://localhost:%s%s/v1/tutorial", port, contextPath), request,
                Long.class);
    }

    private ResponseEntity<ProductItem> getProduct(Long id) {
        return restTemplate.getForEntity(String.format("http://localhost:%s%s/v1/tutorial/{id}", port, contextPath), ProductItem.class, id);
    }

    private ResponseEntity<Void> updateProduct(Long id, UpdateProductRequest request) {
        return restTemplate.exchange(String.format("http://localhost:%s%s/v1/tutorial/{id}", port, contextPath),
                HttpMethod.PUT,
                new HttpEntity<>(request),
                Void.class,
                id);
    }

    private ResponseEntity<Void> updatePartialProduct(Long id, PartialUpdateProductRequest request) {
        return restTemplate.exchange(String.format("http://localhost:%s%s/v1/tutorial/{id}", port, contextPath),
                HttpMethod.PATCH,
                new HttpEntity<>(request),
                Void.class,
                id);
    }

    private ResponseEntity<Void> deleteProduct(Long id){
        return restTemplate.exchange(String.format("http://localhost:%s%s/v1/tutorial/{id}", port, contextPath),
                HttpMethod.DELETE,
                null,
                Void.class,
                id);
    }
}
