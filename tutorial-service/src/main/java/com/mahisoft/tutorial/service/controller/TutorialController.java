package com.mahisoft.tutorial.service.controller;

import com.mahisoft.tutorial.service.controller.dto.*;
import com.mahisoft.tutorial.service.service.TutorialService;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Api(value = "v1/tutorial", description = "Tutorial API")
@RestController
@RequestMapping("v1/tutorial")
@RequiredArgsConstructor
public class TutorialController {

    private final TutorialService service;

    @ApiOperation(value = "Returns a greeting for a given person")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @GetMapping("greeting/{person}")
    public String getGreeting(
            @ApiParam(value = "Name of the person to greet", required = true)
            @PathVariable String person
    ) {
        return service.getGreeting(person);
    }

    @ApiOperation("Returns the information of the given product id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 404, message = "Not Found")
    })
    @GetMapping("/{id}")
    public ProductItem get(
            @ApiParam(value = "The id of the product", required = true )
            @PathVariable Long id
    ) {
        return service.getProductById(id);
    }

    @ApiOperation("Returns the information about all products")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
    })
    @GetMapping("/")
    public ProductItemList getAll(){
        return service.getAllProducts();
    }

    @ApiOperation(value = "A new product should be created with status Active")
    @ApiResponses(value = {
          @ApiResponse(code = 201, message = "Successfully created"),
          @ApiResponse(code = 400, message = "Validation fails")
    })
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Long createProduct(@RequestBody @Validated CreateProductRequest request) {
        return service.createProduct(request);
    }

    @ApiOperation("Update data from a specific product id. Empty response")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Empty Response"),
            @ApiResponse(code = 400, message = "Validation fails"),
            @ApiResponse(code = 404, message = "Not Found")
    })
    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateProduct(
            @ApiParam(value = "Id of the product to update", required = true)
            @PathVariable Long id,

            @ApiParam(value = "Product info to update", required = true)
            @RequestBody @Validated UpdateProductRequest request
    ) {
        service.updateProduct(id, request);
    }

    @ApiOperation("Update partial data from a specific product id. Empty response")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Empty Response"),
            @ApiResponse(code = 400, message = "Validation fails"),
            @ApiResponse(code = 404, message = "Not Found")
    })
    @PatchMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void partialUpdateProduct(
            @ApiParam(value = "Id of the product to update", required = true)
            @PathVariable Long id,

            @ApiParam(value = "Product info to update", required = true)
            @RequestBody @Validated PartialUpdateProductRequest request
    ) {
        service.partialUpdateProduct(id, request);
    }

    @ApiOperation("The Item should be deleted. Only Inactive Products can be deleted")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Empty Response"),
            @ApiResponse(code = 400, message = "Validation fails"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 412, message = "System wasn't prepared for the action")
    })
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(
        @ApiParam(value = "Id of product to delete")
        @PathVariable Long id
    ){
        service.deleteProduct(id);
    }
}
