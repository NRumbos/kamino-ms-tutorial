package com.mahisoft.tutorial.service.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PartialUpdateProductRequest {

    @Size(min = 3, max = 25, message = "Name must have between 3 and 25 characters")
    private String name;

    @Size(min = 3, max = 25, message = "Category must have between 3 and 25 characters")
    private String category;

    private BigDecimal price;

    @Min(value = 10, message = "Discount min value is 10")
    @Max(value = 90, message = "Discount max value is 90")
    private Double discount;

    private StatusType status;

    @SuppressWarnings("unused")
    @AssertTrue(message = "Discount is required when the category is Clearance")
    private boolean isDiscountValid() {
        return this.category == null || !("Clearance".equals(this.category) && this.discount == null);
    }
}
