package com.mahisoft.tutorial.service.service;

import com.google.common.base.Strings;
import com.mahisoft.tutorial.service.controller.dto.*;
import com.mahisoft.tutorial.service.service.domain.ProductEntity;
import com.mahisoft.tutorial.service.service.mapper.ProductMapper;
import com.mahisoft.tutorial.service.service.repository.TutorialDummyRepository;
import com.mahisoft.tutorial.service.service.repository.TutorialRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ValidationException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class TutorialService {

    private final TutorialDummyRepository repository;
    private final TutorialRepository realRepository;

    public String getGreeting(String person) {
        return String.format("Hello World %s!", person);
    }

    public ProductItem getProductById(Long id) {
        return ProductMapper.toDto(
                getProductEntity(id)
        );
    }

    public ProductItemList getAllProducts(){
        Stream<ProductEntity> productEntityList = realRepository.findAll().stream();
        List<ProductItem> productItemList = productEntityList.map(ProductMapper::toDto).collect(Collectors.toList());

        return ProductItemList.builder().products(productItemList).build();
    }

    @Transactional(rollbackFor = Exception.class)
    public Long createProduct(CreateProductRequest request) {
        ProductEntity savedRecord = realRepository.save(
            ProductEntity
                .builder()
                .category(request.getCategory())
                .discount(request.getDiscount())
                .name(request.getName())
                .price(request.getPrice())
                .status(StatusType.Active)
                .build()
        );
        return savedRecord.getId();
    }

    @Transactional(rollbackFor = Exception.class)
    public void updateProduct(Long id, UpdateProductRequest request) {
        ProductEntity product = getProductEntity(id);

        product.setName(request.getName());
        product.setCategory(request.getCategory());
        product.setDiscount(request.getDiscount());
        product.setPrice(request.getPrice());
        product.setStatus(request.getStatus());

        realRepository.save(product);
    }

    @Transactional(rollbackFor = Exception.class)
    public void partialUpdateProduct(Long id, PartialUpdateProductRequest request) {
        ProductEntity original = getProductEntity(id);

        if(!Strings.isNullOrEmpty(request.getName())) {
            original.setName(request.getName());
        }

        if(!Strings.isNullOrEmpty(request.getCategory())) {
            original.setCategory(request.getCategory());
        }

        if(request.getDiscount() != null) {
            original.setDiscount(request.getDiscount());
        }

        if(request.getPrice() != null) {
            original.setPrice(request.getPrice());
        }

        if(request.getStatus() != null) {
            original.setStatus(request.getStatus());
        }

        realRepository.save(original);
    }

    public void deleteProduct(Long id) throws ValidationException {
        ProductEntity product = getProductEntity(id);

        if (product.getStatus() == StatusType.Active) {
            throw new ValidationException(String.format("Product '%s' is active", id));
        }

        realRepository.delete(id);
    }

    private ProductEntity getProductEntity(Long id) {
        ProductEntity product = realRepository.findOne(id);
        if (product == null) {
            throw new NoSuchElementException(String.format("Product '%s' not found.", id));
        }
        return product;
    }
}
